class App {
    constructor() {
        // Assume terminalElement is the HTML element acting as your terminal window
        this.terminalElement = document.getElementById('terminal');
		this.log("Hello from Melo! 👋");
		this.midiAccess = undefined;
    }

	log(message) {
		const maxLines = 100; // Set the maximum number of lines
	
		// Append the new message as a line in the terminal
		const line = document.createElement("p");
		line.textContent = ">> " + message;
		this.terminalElement.appendChild(line);
	
		// Scroll to the bottom
		this.terminalElement.scrollTop = this.terminalElement.scrollHeight;
	
		// Remove oldest lines if exceeding maxLines
		while (this.terminalElement.children.length > maxLines) {
			this.terminalElement.removeChild(this.terminalElement.firstChild);
		}
	}
	
    async testBrowser() {
        this.hr();
        this.log("Checking your browser for MIDI support...");

        try {
            const midiAccess = await navigator.requestMIDIAccess({ sysex: false });
            this.onMIDISuccess(midiAccess);
        } catch (e) {
            this.onMIDIFailure(e);
        }
    }

	getDevices() {
		this.hr();
		this.log("Enumerating MIDI Devices...");
		let i = 0;
		for (var input of this.midiAccess.inputs.values()) {
			i++;
			this.log(`${i}. Device Name: ${input.name}`);
			this.log(`.. Manufacturer: ${input.manufacturer}`);
			this.log(`.. State: ${input.state}`);
			this.log(`.. Connection: ${input.connection}`);
			this.log(`.. ID: ${input.id}`);
			this.log(`.. Version: ${input.version}`);

			// I shouldn't do it here, but I will anyway...
			// Monitor Input Data...
			input.onmidimessage = (msg) => {
				// Extract the data from the MIDI message
				const [command, note, velocity] = msg.data;
			
				// Determine the type of MIDI message
				const messageType = command & 0xF0;
				const channel = (command & 0x0F) + 1; // MIDI channels are 1-16
			
				// Prepare a detailed message
				let detailMessage = `Channel: ${channel}, `;
			
				switch (messageType) {
					case 0x80: // Note Off
						detailMessage += `Note Off, Note: ${note}, Velocity: ${velocity}`;
						break;
					case 0x90: // Note On
						detailMessage += `Note On, Note: ${note}, Velocity: ${velocity}`;
						break;
					case 0xB0: // Control Change
						detailMessage += `Control Change, Controller: ${note}, Value: ${velocity}`;
						break;
					case 0xC0: // Program Change
						detailMessage += `Program Change, Program Number: ${note}`;
						break;
					case 0xE0: // Pitch Bend
						const pitchBend = (velocity << 7) | note; // Combine note and velocity to get the 14-bit pitch bend value
						detailMessage += `Pitch Bend, Value: ${pitchBend}`;
						break;
					default:
						detailMessage += `Unknown Message, Command: ${command}, Data: [${note}, ${velocity}]`;
						break;
				}
			
				// Log the detailed message
				this.log(detailMessage);
			};
			
		}
		return this.midiAccess.inputs;
	}

    onMIDISuccess(midiAccess) {
		this.midiAccess = midiAccess;
		this.log("Your browser supports MIDI! 🌈");
        console.log('MIDI Access Object', midiAccess);
		this.getDevices();
    }

    onMIDIFailure(e) {
        this.log(`No access to MIDI devices or your browser doesn't support WebMIDI API. Please use WebMIDIAPIShim ${e}`);
    }

	hr()
	{
		const rule = document.createElement("hr");
		this.terminalElement.appendChild(rule);
	}

	melo_ipsum() {
		this.hr();
		this.log("Ah, dig it, dig it, I'm Melo the Mole, with a MIDI vibe and a funky soul. Tappin' on keys, underground like a dole, mixin' up beats in my subterranean role. Notes are my dirt, rhythms my coal, I'm mining for grooves that'll take control.");
		this.log("Bleep, bloop, blip, I'm a mole on a mission, with a head full of tunes and a heart full of vision. My paws on the pads, I'm composing with precision, while I'm dodging the roots, in a musical collision. Synth lines so slick, they cause an incision, in the fabric of sound, that's my personal fission.");		
		this.log("Twiddle the knobs, and tweak the dials, I'm layering tracks that go on for miles. With a whisker-wiggle here and a paw-pat there, I'm the mole with the flow and the sonic flair. Underground sound, I'm the critter who cares, making music so fresh, it's the mole's share.");
		this.log("So listen up folks, as I lay down the track, with a kick and a snare that's got no lack. I'm Melo the MIDI Mole, in my sound burrow shack, where the beats are so hot, they could cook a snack. Ipsum to the left, and lorem to the right, I'll keep your toes tapping from day through night!");
	}
}

document.addEventListener("DOMContentLoaded", async () => {
    const melo = new App();
    await melo.testBrowser();
    melo.melo_ipsum();
});
# Melo's MIDI Lab

https://thompsondt.gitlab.io/midi-lab


## Setting Up a Virtual Environment and Installing Dependencies
To run the Python test files in this project, it's important to set up a virtual environment and install the required dependencies. This ensures that you have the correct versions of the libraries needed and that your testing environment is isolated from your global Python environment.

### Creating a Virtual Environment
First, ensure you have Python 3 installed on your system. You can check this by running `python3 --version` in your terminal. If Python 3 is not installed, you should install it from the official Python website.

Once Python 3 is installed, you can create a virtual environment in the project directory. Open your terminal, navigate to the project's root directory, and run the following command:

```bash
python3 -m venv venv
```
This command creates a new directory named venv in your project directory, containing the virtual environment.

### Activating the Virtual Environment
Before you can use the virtual environment, you need to activate it. The activation command differs depending on your operating system:

On macOS and Linux:
```bash
source venv/bin/activate
```

On Windows:
```bash
.\venv\Scripts\activate
```

Once activated, your terminal prompt will change to indicate that you are now working inside the virtual environment.

### Installing Dependencies
With the virtual environment activated, install the project's dependencies by running:

```bash
pip install -r requirements.txt
```

This command reads the `requirements.txt` file and installs all the listed Python modules into your virtual environment.

### Running the Test Files
After setting up the virtual environment and installing the dependencies, you can run the test files. For instance, to run the `test_signal.py` test file, use the following command:

```bash
python test/test_signal.py
```
Remember to deactivate the virtual environment when you're done by simply running deactivate in your terminal.

### Troubleshooting Python Errors

#### ModuleNotFoundError: No module named 'rtmidi'
ref: https://github.com/mido/mido/issues/171
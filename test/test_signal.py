import mido
import time

# Open a MIDI output port (use an available port on your system)
outport = mido.open_output()

# Define ranges for various parameters
note_range = range(0, 127)  # Note numbers
velocity_range = range(0, 127)  # Velocity
channel_range = range(0, 15)  # MIDI channels
cc_range = range(0, 127)  # Control Change values
program_range = range(0, 127)  # Program Change values
pitch_bend_range = range(-8192, 8191)  # Pitch Bend values

# Loop through and send messages for each parameter
try:
    for channel in channel_range:
        for note in note_range:
            # Send Note On and Note Off messages
            outport.send(mido.Message('note_on', channel=channel, note=note, velocity=64))
            time.sleep(0.1)  # Short delay
            outport.send(mido.Message('note_off', channel=channel, note=note, velocity=64))
        
        for cc in cc_range:
            # Send Control Change messages
            time.sleep(0.1)  # Short delay
            outport.send(mido.Message('control_change', channel=channel, control=cc, value=64))

        for program in program_range:
            # Send Program Change messages
            time.sleep(0.1)  # Short delay
            outport.send(mido.Message('program_change', channel=channel, program=program))

        for pitch in pitch_bend_range:
            # Send Pitch Bend messages
            time.sleep(0.1)  # Short delay
            outport.send(mido.Message('pitchwheel', channel=channel, pitch=pitch))

except KeyboardInterrupt:
    print("Exiting loop")
    outport.close()

# Close the port when done
outport.close()